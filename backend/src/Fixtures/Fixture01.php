<?php

include_once __DIR__ . "/../../../common/src/Service/DBConnector.php";

class Fixture01 
{
    private $conn;

    private $data = [
        [
            'id' => 'null',
            'title' => 'Ноутбук',	
            'picture' => '01.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук02',	
            'picture' => '02.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук03',	
            'picture' => '03.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук04',	
            'picture' => '04.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук05',	
            'picture' => '05.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук06',	
            'picture' => '06.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук07',	
            'picture' => '07.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук08',	
            'picture' => '08.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук09',	
            'picture' => '09.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ],
        [
            'id' => 'null',
            'title' => 'Ноутбук10',	
            'picture' => '10.png',	
            'preview' => 'New Asus Laptop N 751 JK',
            'content' => 'LMF;LB;,B 89789798',
            'price' => '7600',	
            'status' => '5',	
            'created' => '2021-01-22 14:27:11',	
            'updated' => '2021-01-22 15:36:35'
        ]
    ];
    public function __construct(DBConnector $conn)
    {
        $this->conn = $conn->connect();
    }
    public function run()
    {
        foreach($this->data as $product){
            copy(__DIR__. "/../../fixtures_pics/" . $product['picture'], __DIR__ . "/../../../uploads/products/" . $product['picture']);

            $result = mysqli_query($this->conn, "INSERT INTO products VALUES (
                " . $product['id'] . ",
                '" . $product['title'] . "',
                '" . $product['picture'] . "',
                '" . $product['preview'] . "',
                '" . $product['content'] . "',
                '" . $product['price'] . "',
                '" . $product['status'] . "',
                '" . $product['created'] . "',
                '" . $product['updated'] . "')");

            if (!$result){
                print mysqli_error($this->conn) . PHP_EOL;
            }
        }
    }
}