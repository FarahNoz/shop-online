<?php
/**
 * Created by PhpStorm.
 * User: noza
 * Date: 02.03.2021
 * Time: 20:49
 */
include_once __DIR__."/Interface/ControllerInterface.php";
include_once __DIR__ . "/../../../common/src/Service/SecurityService.php";

abstract class AbstractController implements ControllerInterface
{
    public function __construct()
    {

        if (!SecurityService::isAuthorized()) {
            header("Location: /?model=site&action=login");
            die();
        }
    }
}